package com.company;

public interface ChatControllerObserver {
    public void update(AbstractChatControllerObservable chatController);
}
