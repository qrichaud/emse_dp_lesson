package com.company;


import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class ChatController extends AbstractChatControllerObservable {
    protected List<ChatMessage> chatContent = new ArrayList<>();
    public final int CHAT_MAX_SIZE = 20;


    public void start() {
        Scanner scanner = new Scanner(System.in);

        while(chatContent.size() < CHAT_MAX_SIZE) {
            System.out.print("Enter message : ");
            String userInput = scanner.nextLine();
            ChatMessage newMessage =  new ChatMessage(userInput);
            this.chatContent.add(newMessage);
            this.notifyObservers();
        }
    }

    @Override
    public List<ChatMessage> getChatContent() {
        return chatContent;
    }

    @Override
    public int getCHAT_MAX_SIZE() {
        return CHAT_MAX_SIZE;
    }


}
