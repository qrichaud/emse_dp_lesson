package com.company;

public class MarkdownRenderChatObserver implements ChatControllerObserver {
    @Override
    public void update(AbstractChatControllerObservable chatController) {
        String res = "";
        for (ChatMessage message : chatController.getChatContent()) {
            res += String.format("**%s**: %s\n", message.getAuthor(), message.getContent());
        }
        System.out.println(res);
    }
}
